select *
from shippers;

select shippers.shipperName, count(orderID)
from orders join shippers on orders.shipperID = shippers.shipperID
group by orders.shipperID;

select customerName, OrderID, customers.customerID, orders.customerID as ordersCustomerID, count(orders.orderID)
from customers left join orders on (customers.CustomerID = orders.CustomerID)
group by customerID;

select firstName, lastName, orderID
from orders right join employees on (orders.EmployeeID = employees.employeeID)
order by orderID;

select *
from Customers;

create view mexicanCustomers as
select customerId, customername, contactname
from customers
where country = 'Mexico';

 select *
 from mexicanCustomers;
 
 select *
 from mexicancustomers join orders on mexicancustomers.customerId = orders.CustomerID;
 
 create view productsbelowavg as 
 select productId, productName, price 
 from products
 where price < (select avg(price) from products);
 
 delete from customers;
 truncate customers;
 
 drop table customers;
 
 
 
 
 
 
 