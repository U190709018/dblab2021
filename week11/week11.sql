/*call getCustomersByCountry('USA');

set @Country = 'UK';
select @Country;
call getCustomersByCountry(@Country);
select @Country;

select * from Shippers;

select count(OrderID)
from Orders join Shippers on Orders.shipperID = Shippers.ShipperID
where ShipperName = 'Speedy Express';

set @orderCount = 0;
call getNumberOfOrdersByShipper('Speedy Express', @orderCount);
select @orderCount;

set @beg = 100;
set @inc = 10;

call counter(@beg, @inc);
select @beg;
select @inc;
*/
select * from movies;
select * from denormalized;

load data infile "/home/student/Downloads/denormalized.csv"
into table denormalized
columns terminated by ';';

show variables like "secure_file_priv";

insert into movies(movie_id, title, ranking, rating, year, votes, duration, oscars, budget) 
select distinct movie_id, title, ranking, rating, year, votes, duration, oscars, budget
from denormalized;

select * from movies;

insert into countries(country_id, country_name)
select distinctrow producer_country_id, producer_country_name
from denormalized

union

select distinctrow director_country, director_country_name
from denormalized

union 

select distinctrow star_country_id, star_country_name
from denormalized
Order By producer_country_id;

select * from countries;
